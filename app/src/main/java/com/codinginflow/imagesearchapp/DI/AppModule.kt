package com.codinginflow.imagesearchapp.DI

import com.codinginflow.imagesearchapp.API.UnSplashApi
import dagger.Module
import dagger.Provides
import dagger.hilt.InstallIn
import dagger.hilt.android.components.ApplicationComponent
import dagger.hilt.components.SingletonComponent
import retrofit2.Retrofit
import retrofit2.converter.gson.GsonConverterFactory
import javax.inject.Singleton

@Module
@InstallIn(ApplicationComponent::class)
object AppModule {

    @Provides
    @Singleton
    fun provideRetrofit(): Retrofit =
        Retrofit.Builder()
            .baseUrl(UnSplashApi.BASE_URL)
            .addConverterFactory(GsonConverterFactory.create())
            .build()

    @Provides
    @Singleton
    fun provideUnSplashApi(retrofit: Retrofit): UnSplashApi =
        retrofit.create(UnSplashApi::class.java)

}



//Dagger is a fully static, compile-time dependency injection framework for Java, Kotlin, and Android.
//A Dagger module is a class that is annotated with @Module .
// There, you can define dependencies with the @Provides annotation, Kotlin Java. @provides tells dagger how to create an object
// @Module informs Dagger that this class is a Dagger Module.
//Hilt modules are standard Dagger modules that have an additional @InstallIn annotation that determines which Hilt component(s) to install the module into.
//A Hilt component that has the lifetime of the application. = import dagger.hilt.android.components.ApplicationComponent
