package com.codinginflow.imagesearchapp.API

import androidx.viewbinding.BuildConfig
import retrofit2.http.GET
import retrofit2.http.Headers
import retrofit2.http.Query

interface UnSplashApi {
    companion object{
        const val BASE_URL = "https://api.unsplash.com/"
        const val CLIENT_ID = com.codinginflow.imagesearchapp.BuildConfig.UNSPLASH_ACCESS_KEY
    }

    @Headers("Accept-Version: v1","Authorization: Client-ID $CLIENT_ID")
    @GET("search/photos")
    suspend fun searchPhotos(
        @Query ("query")queryprm : String,
        @Query ("page")pageprm : Int,
        @Query ("per_page")perPageprm : Int
    ):UnSplashResponse

}



//A suspending function is simply a function that can be paused and resumed at a later time.
//They can execute a long running operation and wait for it to complete without blocking.
//The syntax of a suspending function is similar to that of a regular function except for the addition of the suspend keyword.