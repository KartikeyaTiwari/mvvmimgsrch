package com.codinginflow.imagesearchapp.API

import com.codinginflow.imagesearchapp.Data.UnSplashPhoto

data class UnSplashResponse (
    val results : List<UnSplashPhoto>
)