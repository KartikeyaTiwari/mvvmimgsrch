package com.codinginflow.imagesearchapp.UI.Gallery

import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.core.view.isVisible
import androidx.paging.LoadState
import androidx.paging.LoadStateAdapter
import androidx.recyclerview.widget.RecyclerView
import com.codinginflow.imagesearchapp.databinding.UnsplashPhotoLoadStateFooterBinding

class UnSplashPhotoLoadStateAdapter(private val retry : () -> Unit) :
    LoadStateAdapter<UnSplashPhotoLoadStateAdapter.LoadStateViewholder> (){

    override fun onCreateViewHolder(parent: ViewGroup, loadState: LoadState): LoadStateViewholder {
        val binding = UnsplashPhotoLoadStateFooterBinding.inflate(
            LayoutInflater.
            from(parent.context),
            parent,
            false)

        return LoadStateViewholder(binding)
    }

    override fun onBindViewHolder(holder: LoadStateViewholder, loadState: LoadState) {
        holder.bind(loadState)
    }

  inner  class LoadStateViewholder(private val binding : UnsplashPhotoLoadStateFooterBinding):
        RecyclerView.ViewHolder(binding.root){

        init {
            binding.retryBtnidr.setOnClickListener {
                retry.invoke()
            }
        }

        fun bind(loadState: LoadState){
            binding.apply {
                progressBarIdr.isVisible = loadState is LoadState.Loading
                retryBtnidr.isVisible = loadState !is LoadState.Loading
                textViewEmptyIdr.isVisible = loadState !is LoadState.Loading

            }
        }

    }
}


///In Java if we want that a function does return nothing we use void , Unit is the equivalent in Kotlin.