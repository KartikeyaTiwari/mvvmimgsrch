package com.codinginflow.imagesearchapp.UI.Gallery

import androidx.hilt.Assisted
import androidx.hilt.lifecycle.ViewModelInject
import androidx.lifecycle.*
import androidx.paging.cachedIn
import com.codinginflow.imagesearchapp.Data.UnSplashRepository

class GalleryViewModel
    @ViewModelInject
    constructor(
        private val repository: UnSplashRepository,
        @Assisted state : SavedStateHandle
    ) : ViewModel() {

        private val currentQuery = state.getLiveData(CURRENT_QUERY, Default_Query)

        val photos = currentQuery.switchMap {
                queryString -> repository.getSearchResults(queryString).cachedIn(viewModelScope)
        }

        fun searchPhotos(querry : String){
            currentQuery.value = querry
        }

    companion object{
        private const val CURRENT_QUERY = "current_qry"
        private const val Default_Query = "cats"
    }

}


//MutableLiveData is a subclass of LiveData which is used for some of it's properties (setValue/postValue)
// and using these properties we can easily notify the ui when onChange() is called.
// Only using LiveData object we can't do this