package com.codinginflow.imagesearchapp.UI.Gallery

import android.os.Bundle
import android.view.Menu
import android.view.MenuInflater
import android.view.View
import androidx.appcompat.widget.SearchView
import androidx.core.view.isVisible
import androidx.fragment.app.Fragment
import androidx.fragment.app.viewModels
import androidx.lifecycle.observe
import androidx.navigation.fragment.findNavController
import androidx.paging.LoadState
import com.codinginflow.imagesearchapp.Data.UnSplashPhoto
import com.codinginflow.imagesearchapp.R
import com.codinginflow.imagesearchapp.databinding.FragmentGalleryBinding
import dagger.hilt.android.AndroidEntryPoint

@AndroidEntryPoint
class GalleryFragment : Fragment(R.layout.fragment_gallery),UnSplashPhotoAdapter.OnItemClickListner {
    private val viewModel by viewModels<GalleryViewModel>()

    private var _binding: FragmentGalleryBinding? = null
    private val binding get() = _binding!!

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        _binding = FragmentGalleryBinding.bind(view)

        val adapter = UnSplashPhotoAdapter(this)
        binding.apply {
            recyclerViewid.setHasFixedSize(true)
            recyclerViewid.adapter = adapter.withLoadStateHeaderAndFooter(
                header = UnSplashPhotoLoadStateAdapter{
                    adapter.retry()
                },
                footer = UnSplashPhotoLoadStateAdapter{
                    adapter.retry()
                }
            )
            retryBtnid.setOnClickListener {
                adapter.retry()

            }
        }

        viewModel.photos.observe(viewLifecycleOwner) {
            adapter.submitData(viewLifecycleOwner.lifecycle, it)
        }

        adapter.addLoadStateListener { loadState ->
            binding.apply {
                progressBarId.isVisible = loadState.source.refresh is LoadState.Loading
                recyclerViewid.isVisible = loadState.source.refresh is LoadState.NotLoading
                retryBtnid.isVisible = loadState.source.refresh is LoadState.Error
                textViewErrorId.isVisible = loadState.source.refresh is LoadState.Error

                //empty view
//                if(loadState.source.refresh is LoadState.NotLoading &&
//                        loadState.append.endOfPaginationReached &&
//                        adapter.itemCount>1){
//                    recyclerViewid.isVisible = false
//                    textViewEmptyId.isVisible = true
//                }else{
//                    textViewErrorId.isVisible = false
//
//                }
            }
        }

        setHasOptionsMenu(true)
    }

    override fun onItemClick(photo: UnSplashPhoto) {
        val action = GalleryFragmentDirections.actionGalleryFragmentToDetaialFragment(photo)
        findNavController().navigate(action)
    }

    override fun onCreateOptionsMenu(menu: Menu, inflater: MenuInflater) {
        super.onCreateOptionsMenu(menu, inflater)
        inflater.inflate(R.menu.menu_gallery, menu)
        val searchItem = menu.findItem(R.id.action_search)
        val searchView = searchItem.actionView as SearchView

        searchView.setOnQueryTextListener(object : SearchView.OnQueryTextListener {
            override fun onQueryTextSubmit(query: String?): Boolean {
                  if (query != null){
                      binding.recyclerViewid.scrollToPosition(0)
                      viewModel.searchPhotos(query)
                      searchView.clearFocus()
                  }
                return true
            }

            override fun onQueryTextChange(newText: String?): Boolean {
                return true
            }
        })
    }

    override fun onDestroyView() {
        super.onDestroyView()
        _binding = null
    }

}