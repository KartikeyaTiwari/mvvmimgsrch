package com.codinginflow.imagesearchapp.Data

import androidx.paging.PagingSource
import com.codinginflow.imagesearchapp.API.UnSplashApi
import retrofit2.HttpException
import java.io.IOException

private const val UNSPLASH_STARTING_INDEX = 1

class UnSplashPagingSource(
    private val unSplashApi: UnSplashApi,
    private val query: String

    ) : PagingSource<Int, UnSplashPhoto>() {
            override suspend fun load(params: LoadParams<Int>): LoadResult<Int, UnSplashPhoto> {
            val position = params.key ?: UNSPLASH_STARTING_INDEX

               return try {
                    val response = unSplashApi.searchPhotos(query,position, params.loadSize)
                    val photos =    response.results

                    LoadResult.Page(data = photos,
                    prevKey = if(position == UNSPLASH_STARTING_INDEX)null else position-1,
                    nextKey = if (photos.isEmpty())null else position + 1)
                }catch (exception : IOException){
                    LoadResult.Error(exception)
                }catch (exceptionn : HttpException){
                   LoadResult.Error(exceptionn)
               }
            }
    }



//Paging is a part of the Android Jetpack and is used to load and display small amounts of data from the remote server.
//By doing so, it reduces the use of network bandwidth.
// Some of the advantages of Paging can be: ...You will get the content on the page faster, It uses very less memory
//It doesn't load the useless data i.e. only one or two pages can be loaded at a time
//You can use Paging along with LiveData and ViewModel to observe and update data in an easier way.

//paging source know how to load data from api and then turn into pages
//use paging source to construct paging data which we later feed to recylerview adapter

// https://blog.mindorks.com/implementing-paging-library-in-android#:~:text=Paging%20is%20a%20part%20of,advantages%20of%20Paging%20can%20be%3A&text=It%20doesn't%20load%20the,be%20loaded%20at%20a%20time.